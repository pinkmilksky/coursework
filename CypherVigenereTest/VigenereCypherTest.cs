using CourseWork.Models;
using System;
using Xunit;

namespace VigenereCypherTest
{
    public class VigenereCypherTest
    {
        [Fact]

        public void Decypher()
        {

            VigenereCypher vc = new VigenereCypher();
            string two = vc.Decypher("�����������6 4qwhf____??����.. ...+�����", "����");
            Assert.Equal("������������6 4qwhf____??����.. ...+�����", two);
                                                                      
        }

        [Fact]

        public void Cypher()
        {
            VigenereCypher vc = new VigenereCypher();
            string one = vc.Cypher("�����������6 4qwhf____??����.. ...+�����", "����");
            Assert.Equal("�����������6 4qwhf____??����.. ...+�����", one);
        }
    }
}
