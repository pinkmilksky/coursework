﻿using System;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork.Models
{
    public class FileProcessing
    {
        public string Read(Stream s, string fileName)
        {
            if (fileName.EndsWith(".txt"))
            {
                using (var reader = new StreamReader(s))
                {
                    return reader.ReadToEnd();
                }
            }
            else if (fileName.EndsWith(".docx"))
            {
                return ReadDocx(s);
            }
            else
            {
                throw new FileFormatException();
            }
        }

        public string ReadDocx(Stream stream)
        {
            using (var wordDocument = WordprocessingDocument.Open(stream, false))
            {
                var text = wordDocument.MainDocumentPart.Document.Body.InnerText;
                return text;
            }

        }

        public byte[] Write(string text, string fileName)
        {
            if (fileName.EndsWith(".txt"))
            {
                return new UTF8Encoding(true).GetBytes(text);
            }
            else if (fileName.EndsWith(".docx"))
            {
                return WriteDocx(text);
            }
            else
            {
                throw new FileFormatException();
            }
        }

        public byte[] WriteDocx(string text)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Create(stream, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = wordDoc.AddMainDocumentPart();

                    mainPart.Document = new Document();
                    Body body = mainPart.Document.AppendChild(new Body());
                    Paragraph para = body.AppendChild(new Paragraph());
                    Run run = para.AppendChild(new Run());

                    run.AppendChild(new Text(text));
                }
                return stream.ToArray();
            }
        }

    }
}
