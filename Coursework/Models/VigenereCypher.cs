﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CourseWork.Models
{
    public class VigenereCypher
    {
        static string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        public string Decypher(string text, string key)
        {
            return DoMagic(text, key, false);

        }

        public string Cypher(string text, string key)
        {
            return DoMagic(text, key, true);
        }

        private string DoMagic(string text, string key, bool isCypher)
        {

            string newText = null;
            int count = 0;
            for (int i = 0; i < text.Length; i++)
            {
               
                key = key.ToLower();

                foreach (var item in key)
                {
                    if (Char.IsNumber(item))
                    {
                        throw new Exception();
                    }
                    
                }
                    


                if (Regex.Matches(key, @"[a-z]").Count > 0) throw new Exception();
                if (!alphabet.Contains(Char.ToLower(text[i])))
                {
                    newText += text[i];
                }
                else
                {
                    bool isLower = Char.IsLower(text[i]);

                    int index;
                    if (isCypher)
                    {
                        index = alphabet.IndexOf(Char.ToLower(text[i])) + alphabet.IndexOf(key[count]);
                    }
                    else
                    {
                        index = alphabet.IndexOf(Char.ToLower(text[i])) + alphabet.Length - alphabet.IndexOf(key[count]);
                    }
                    index %= alphabet.Length;
                    if (isLower)
                    {
                        newText += alphabet[index];
                    }
                    else
                    {
                        newText += Char.ToUpper(alphabet[index]);
                    }
                    count++;
                    if (count == key.Length) { count = 0; }

                }

            }

            return newText;
        }
    }
}
