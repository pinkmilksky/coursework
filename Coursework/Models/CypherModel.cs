﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CourseWork.Models
{
    public class CypherModel
    {
        public string Text { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string Action { get; set; } = "cypher";
        public IFormFile TextFile { get; set; } = null;

        public bool IsTextInput { get; set; } = true;
    }
}
