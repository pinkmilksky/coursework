﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CourseWork.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;

namespace CourseWork.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private VigenereCypher cypher = new VigenereCypher();
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            
            return View(new CypherModel());
        }

        [HttpPost]
        public IActionResult Index(CypherModel model, IFormFile file)
        {
            
            string text = model.Text;
            
            string result = null;
            try
            {
                if (model.Action == "cypher")
                {
                    result = cypher.Cypher(text, model.Key);
                }
                else if (model.Action == "decypher")
                {
                    result = cypher.Decypher(text, model.Key);
                }
            }
            catch
            {
                ViewBag.ErrorMessage = "Ошибка: ключ должен содержать только русские буквы";
                
                return View(new CypherModel());
            }

            ViewBag.Text = result;
            return View();
        }

        [HttpGet]
        public IActionResult Privacy()
        {
            return View(new CypherModel());
        }

        [HttpPost]
        public IActionResult Privacy(CypherModel model, IFormFile file)
        {
            FileProcessing fileProcessing = new FileProcessing();
            string text;
            string result = "";
            try
            {
                text = fileProcessing.Read(file.OpenReadStream(), file.FileName);
            }
            catch
            {
                ViewBag.ErrorMessage = "Ошибка: неподдерживаемый формат файла";
                return View(new CypherModel());
            }


            try
            {
                if (model.Action == "cypher")
                {
                    result = cypher.Cypher(text, model.Key);
                }
                else if (model.Action == "decypher")
                {
                    result = cypher.Decypher(text, model.Key);
                }
            }
            catch
            {
                ViewBag.ErrorMessage = "Ошибка: ключ должен содержать только русские буквы";

                return View(new CypherModel());
            }
            byte[] info = fileProcessing.Write(result, file.FileName);
            string contentType = "text/plain";
            if (file.FileName.EndsWith(".docx"))
            {
                contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            }
            return File(info, contentType, file.FileName);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
